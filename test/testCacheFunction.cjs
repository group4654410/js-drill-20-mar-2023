const cacheFunction = require("../cacheFunction.cjs");
const cb = (x, y) => { return "cb"; }

const a = cacheFunction(cb);

try {
    console.log(a(1, 2, 3, 4));
    console.log(a(1, 2, 3, 4));
    console.log(a(1, 2, 3));
    console.log(a(1, 2, 3, 4));
} catch (error) {
    console.log(error);
}
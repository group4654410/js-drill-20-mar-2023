const counterFactory = require("../counterFactory.cjs");
let counts = counterFactory();

console.log(counts.increment());
console.log(counts.decrement());
console.log(counts.increment());
const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");
const cb = (z) => { console.log(z + 5); return z; }
let limitedFunction = limitFunctionCallCount(cb, 3);

try {
    console.log(limitedFunction(12));
    console.log(limitedFunction(12));
    console.log(limitedFunction(12));
    console.log(limitedFunction(12));
} catch (error) {
    console.log(error);
}
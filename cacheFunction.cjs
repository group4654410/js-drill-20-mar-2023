function cacheFunction(cb) {

    let results = {};

    if (typeof cb !== 'function') {
        throw new Error("Incorrect arguments");
    }

    return (...args) => {

        if (results[JSON.stringify(args)]) {
            return results[JSON.stringify(args)];
        }

        results[JSON.stringify(args)] = cb(...args);
        return results[JSON.stringify(args)];

    }
}

module.exports = cacheFunction;
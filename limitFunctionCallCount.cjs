function limitFunctionCallCount(cb, n) {
    
    let count = n;

    if (typeof cb !== 'function' || typeof n !== 'number' || arguments.length !== 2) {
        throw new Error("Incorrect arguments");
    }
    
    return (...args) => {

        if (count > 0) {
            count--;
            return cb(...args);
        }
        return null;
    }
}

module.exports = limitFunctionCallCount;